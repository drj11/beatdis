package main

import (
	"fmt"
	"github.com/gordonklaus/portaudio"
	"math"
	"math/rand"
	"os"
	"time"
)

func Noise() (chan<- bool, func([]float32)) {
	var on bool
	noise := func(out []float32) {
		for i := range out {
			out[i] = rand.Float32()*2 - 1
		}
	}
	silence := func(out []float32) {
		for i := range out {
			out[i] = 0
		}
	}

	control := make(chan bool)

	go func(next <-chan bool) {
		for {
			on = <-next
			rand.Seed(1)
		}
	}(control)

	return control, func(out []float32) {
		if on {
			noise(out)
		} else {
			silence(out)
		}
	}
}

func main() {
	portaudio.Initialize()
	defer portaudio.Terminate()
	h, err := portaudio.DefaultHostApi()
	if err != nil {
		panic(err)
	}

	params := portaudio.HighLatencyParameters(nil, h.DefaultOutputDevice)
	params.Output.Channels = 1
	// Enough sample for 1ms
	params.FramesPerBuffer = int(math.RoundToEven(
		params.SampleRate * 1 / 1000))

	control, filbuf := Noise()
	stream, err := portaudio.OpenStream(params, filbuf)
	if err != nil {
		panic(err)
	}
	defer stream.Close()

	bpm := float32(114)
	if len(os.Args) > 1 {
		fmt.Sscan(os.Args[1], &bpm)
	}

	err = stream.Start()
	if err != nil {
		panic(err)
	}

	go beat(control, bpm)

	<-(chan struct{})(nil)
}

func beat(control chan<- bool, bpm float32) {
	p := time.Duration(60.0*1000/bpm) * time.Millisecond
	mark := p / 4
	max := 90 * time.Millisecond
	if mark > max {
		mark = max
	}
	space := p - mark

	for {
		control <- false
		time.Sleep(space)
		control <- true
		time.Sleep(mark)
	}
}
